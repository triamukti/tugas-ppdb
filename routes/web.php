<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("sync-cabang-siswa", "CabangController@sync_siswa");
Route::get("sync-cabang-sekolah", "CabangController@sync_sekolah");
Route::get("sync-cabang-nilai", "CabangController@sync_nilai");
Route::get("pull-tujuan-sekolah-dari-pusat", "CabangController@pull_tujuan_sekolah");


Route::get("pusat-pull-tujuan-sekolah", "PusatController@pull_tujuan_sekolah");