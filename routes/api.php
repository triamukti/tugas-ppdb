<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::GET('/sync-data', "SyncController@sync");

Route::GET('/sync-siswa', "SyncController@sync_siswa");
Route::GET('/sync-sekolah', "SyncController@sync_sekolah");
Route::GET('/sync-nilai', "SyncController@sync_nilai");
Route::GET('/pull-tujuan-sekolah', "SyncController@pull_tujuan_sekolah");

Route::GET("/sync-tujuan-sekolah", "SyncController@sync_tujuan_sekolah");
