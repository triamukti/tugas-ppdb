<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTujuanSekolah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tujuansekolah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siswa_id');
            $table->string('siswa_nisn');
            $table->integer('pilihan_sekolah_id');
            $table->string('pilihan_sekolah_npsn');
            $table->integer('pilihan_ke');
            $table->enum('status', ["pending", "diterima", "ditolak", "cancel"])->default("pending");
            $table->boolean("sync");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tujuansekolah');
    }
}
