<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSekolah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kota_id');
            $table->integer('propinsi_id');
            $table->string('nama');
            $table->string('npsn')->nullable();
            $table->text('alamat');
            $table->string('no_telp')->nullable();
            $table->enum('tingkat', ["SMP/MTs", "SMA/SMK/MA"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolah');
    }
}
