<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("sekolah")->insert([
        	// SMP
			["kota_id" => 1,"npsn"=> "9876543211" ,"propinsi_id" => 1,"nama" => "SMP Negeri 1","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMP/MTs",],
			["kota_id" => 2,"npsn"=> "9876543212" ,"propinsi_id" => 2,"nama" => "SMP Negeri 2","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMP/MTs",],
			["kota_id" => 3,"npsn"=> "9876543213" ,"propinsi_id" => 3,"nama" => "SMP Negeri 3","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMP/MTs",],
			["kota_id" => 4,"npsn"=> "9876543214" ,"propinsi_id" => 4,"nama" => "SMP Negeri 4","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMP/MTs",],
			["kota_id" => 5,"npsn"=> "9876543215" ,"propinsi_id" => 5,"nama" => "SMP Negeri 5","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMP/MTs",],

        	// SMA
        	["kota_id" => 6, "npsn"=> "987765432466","propinsi_id" => 6,"nama" => "SMA Negeri 6","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 7, "npsn"=> "987765432467","propinsi_id" => 7,"nama" => "SMA Negeri 7","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 8, "npsn"=> "987765432468","propinsi_id" => 8,"nama" => "SMA Negeri 8","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 9, "npsn"=> "987765432469","propinsi_id" => 9,"nama" => "SMA Negeri 9","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 10, "npsn"=> "9877654324610","propinsi_id" => 10,"nama" => "SMA Negeri 10","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],

        	// SMK
        	["kota_id" => 11, "npsn"=> "9877654324611","propinsi_id" => 11,"nama" => "SMK Negeri 11","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 12, "npsn"=> "9877654324612","propinsi_id" => 12,"nama" => "SMK Negeri 12","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 13, "npsn"=> "9877654324613","propinsi_id" => 13,"nama" => "SMK Negeri 13","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 14, "npsn"=> "9877654324614","propinsi_id" => 14,"nama" => "SMK Negeri 14","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        	["kota_id" => 15, "npsn"=> "9877654324615","propinsi_id" => 15,"nama" => "SMK Negeri 15","alamat" => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","no_telp" => "123456789","tingkat" => "SMA/SMK/MA",],
        ]);
    }
}
