<?php

use Illuminate\Database\Seeder;

class MatpelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("matpel")->insert([
        	["nama"=> "Matematika"],
        	["nama"=> "Bahasa Indonesia"],
        	["nama"=> "Bahasa Inggris"],
        	["nama"=> "IPA"],
        	["nama"=> "Pendidikan Agama"],
        	["nama"=> "PPKN"],
        	["nama"=> "IPS"],
        ]);
    }
}
