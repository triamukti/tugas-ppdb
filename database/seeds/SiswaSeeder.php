<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create("id_ID");
    	foreach (range(1,100) as $index) {
    		DB::table("siswa")->insert([
    			[
                "nama" => $faker->firstName." ".$faker->lastName,
    			"nisn" => "999".$faker->randomNumber(4, true),
    			"alamat" => $faker->address,
    			"no_telp" => $faker->phoneNumber,
    			"asal_sekolah_id" => rand(1,5),
    			]
    		]);
    	}
    }
}
