<?php

namespace App\Library;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\Response;

/**
 * Kelas untuk memanggil fungsi-fungsi API yang ada di sistem
 */
class API
{
	public static function set_client_pusat()
	{
		$base_url = "http://ppdb.local/";

		$client = new Client(["base_uri"=> $base_url, 'http_errors' => false]);

		return $client;
	}

	public static function set_client_cabang($host)
	{
		$base_url = $host;

		$client = new Client(["base_uri"=> $base_url, 'http_errors' => false]);

		return $client;
	}

	public static function sync_siswa()
	{
		$uri = "/api/sync-siswa";

		$client = self::set_client_pusat();
		$request = $client->request("GET", $uri);
		if ($request->getStatusCode() == 200) {
			return $request->getBody();
		}else{
			return false;
		}
	}

	public static function sync_sekolah()
	{
		$uri = "/api/sync-sekolah";

		$client = self::set_client_pusat();
		$request = $client->request("GET", $uri);
		if ($request->getStatusCode() == 200) {
			return $request->getBody();
		}else{
			return false;
		}
	}

	public static function sync_nilai()
	{
		$uri = "/api/sync-nilai";

		$client = self::set_client_pusat();
		$request = $client->request("GET", $uri);
		if ($request->getStatusCode() == 200) {
			return $request->getBody();
		}else{
			return false;
		}
	}

	//
	public static function pull_tujuan_sekolah($npsn_sekolah)
	{
		$uri = "/api/pull-tujuan-sekolah";
		$client = self::set_client_pusat();

		$query = [
			"query" => ["npsn" => $npsn_sekolah]
		];

		$request = $client->request("GET", $uri, $query);
		if ($request->getStatusCode() == 200) {
			return $request->getBody();
		}else{
			return false;
		}
	}

	public static function pull_tujuan_sekolah_pusat($host)
	{
		$uri = "/api/sync-tujuan-sekolah";
		$client = self::set_client_cabang($host);

		$request = $client->request("GET", $uri);
		if ($request->getStatusCode() == 200) {
			return $request->getBody();
		}else{
			return false;
		}
	}
}


 ?>