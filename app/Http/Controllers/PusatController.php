<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Library\API;

class PusatController extends Controller
{
    public function pull_tujuan_sekolah()
    {
    	// Setup Tujuan Host
    	$host = "http://ppdb.local"; #ini nanti dijadiin dynamic

    	// Akses API
    	$pull_data = API::pull_tujuan_sekolah_pusat($host);
    	$pull_data == false ? dd("Error") : $decode = json_decode($pull_data, 1);

    	// Masukkin ke DB
    	DB::table("tujuan_sekolah")->insert($decode);

    	return "Pusat berhasil ambil data dari Cabang";
    }
}
