<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory as Faker;
use DB;

use App\TujuanSekolah;
use App\Sekolah;
use App\Siswa;

class SyncController extends Controller
{
    public function sync()
    {
        $nama_prov = env("PROV_NAME");
        $provinsi = DB::table("states")->where("state_name", $nama_prov)->first();
    	$tujuan_sekolah = TujuanSekolah::join("sekolah", "sekolah.id", "=", "tujuan_sekolah.pilihan_sekolah_id")
        ->where("propinsi_id", $provinsi->state_id)
        ->get()->toArray();

        $data = [
            "data"=> $tujuan_sekolah
        ];

        return response()->json($data, "200");
    }

    public function sync_siswa()
    {
        $siswa = DB::table("siswa")
        ->select("nisn", "asal_sekolah_id", "nama", "alamat", "no_telp")
        ->get();

        return response()->json($siswa, 200);
    }

    public function sync_sekolah()
    {
        $sekolah = DB::table("sekolah")
        ->select("npsn", "nama", "alamat", "no_telp", "kota_id", "provinsi_id", "tingkat", "user_id")
        ->get();

        return response()->json($sekolah, 200);
    }

    public function sync_nilai()
    {
        // $nilai_siswa = Nilai::with("matpel")->all();
        $nilai_siswa = DB::table("nilai")
        ->select("nisn", "mata_pelajaran_id", "nilai")
        ->get();

        return response()->json($nilai_siswa, 200);
    }

    public function sync_tujuan_sekolah()
    {
        // Ambil data tujuan sekolah
        $tujuan_sekolah = DB::table("tujuan_sekolah")
        ->select("nisn", "npsn", "pilihan_ke", "status", "siswa_id", "sekolah_id")
        ->get();

        return response()->json($tujuan_sekolah, 200);
    }

    public function pull_tujuan_sekolah(Request $request)
    {
        $data = $request->all();
        $npsn_sekolah = $data["npsn"];

        // Ambil data tujuan sekolah
        $tujuan_sekolah = DB::table("tujuan_sekolah")
        ->select("nisn", "npsn", "pilihan_ke", "status", "siswa_id", "sekolah_id")
        ->where(["npsn"=> $npsn_sekolah])
        ->get();

        return response()->json($tujuan_sekolah, 200);
    }
}
