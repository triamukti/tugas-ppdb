<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Library\API;

class CabangController extends Controller
{
    public function sync_siswa()
    {
    	// Ambil data siswa di pusat lewat API
    	$data_siswa = API::sync_siswa();
    	if ($data_siswa == false) { dd("Error 404"); }
    	$decode_data_siswa = json_decode($data_siswa, 1);

    	// drop data sebelumnya
    	$drop_table_siswa = DB::table("siswa")->truncate();

    	// insert data dari pusat
    	DB::table("siswa")->insert($decode_data_siswa);

    	return "Sync Siswa Berhasil";
    }

    public function sync_sekolah()
    {
    	// Ambil data sekolah di pusat lewat API
    	$data_sekolah = API::sync_sekolah();
    	if ($data_sekolah == false) { dd("Error 404"); }
    	$decode_data_sekolah = json_decode($data_sekolah, 1);

    	// Drop data sebelumnya
    	$drop_table_sekolah = DB::table("sekolah")->truncate();

    	// Insert data dari pusat
    	DB::table("sekolah")->insert($decode_data_sekolah);

    	return "Sync Sekolah Berhasil";
    }

    public function sync_nilai()
    {
    	// Ambil data nilai di pusat lewat API
    	$data_nilai = API::sync_nilai();
    	if ($data_nilai == false) { dd("Error 404"); }
    	$decode_data_nilai = json_decode($data_nilai, 1);

    	// Drop data sebelumnya
    	$drop_table_nilai = DB::table("nilai")->truncate();

    	// Insert data dari pusat
    	DB::table("nilai")->insert($decode_data_nilai);

    	return "Sync Nilai Siswa Berhasil";
    }

    /*Ambil list siswa + datanya yang mendaftar ke sekolah yang request API ini*/
    public function pull_tujuan_sekolah()
    {
    	// Setup Host dan NPSN sekolah
    	$npsn_sekolah = "1110123471048"; #nanti dibuat pake env atau gimana terserah

    	// Akses API
    	$pull_data = API::pull_tujuan_sekolah($npsn_sekolah);
    	$pull_data == false ? dd("Error") : $decode_data = json_decode($pull_data, 1);

    	// Masukkin Data ke DB
    	DB::table("tujuan_sekolah")->insert($decode_data);
    	return "Berhasil Ambil Siswa dari Pusat ke table Tujuan sekolah";
    }
}
