<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TujuanSekolah extends Model
{
    protected $table = "tujuansekolah";

    protected $fillable = ["*"];

    public function siswa()
    {
    	return $this->belongsTo("App\Siswa");
    }

   public function pilihan_sekolah()
   {
   		return $this->belongsTo("App\Sekolah", "pilihan_sekolah_id");
   }
}
